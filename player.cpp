#include "player.h"
#include <cstdlib>
#include <iostream>
#include <map>
#include <vector>

/*
 * hi chimney
 * hi kim
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    // Create a board. 
    board = new Board;
    
    // Set the side.
    pside = side;
    otherside = (side == BLACK) ? WHITE : BLACK;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
	
	
	board->doMove(opponentsMove, otherside); 
	/*
	 * SIMPLE AI (selects first move it finds as valid) 
	 *
	 
	for(int x = 0; x < 8; x++)
	{
		for(int y = 0; y < 8; y++)
		{
			// Pick a square incrementally to try.
			std::cerr << x << " " << y << std::endl;
			Move *new_move = new Move(x, y);
			
			// Check if valid.
			if (board->checkMove(new_move, pside))
			{
				std::cerr << "Found a move" << std::endl;
				board->doMove(new_move, pside);
				return new_move;
			}
		}
	}
	
	std::cerr << "No moves found" << std::endl;
	board->doMove(NULL, pside);
	return NULL;	
	*/
	
	std::map<Move*, int> heuristic_map;	

	for(int x = 0; x < 8; x++)
	{
		for(int y = 0; y < 8; y++)
		{
			// Pick a square incrementally to try.
			Move *new_move = new Move(x, y);
			
			// Check if valid.
			if (board->checkMove(new_move, pside))
			{
				//Try out move on temporary board
				Board* temp_board = board->copy();
				
				temp_board->doMove(new_move, pside);
				
				// Make a vector to hold the values of the opp's moves.
				std::vector<int> opp_value;
				
				// Make all possible moves on this board and find the
				// minimum value. 
				for (int i = 0; i < 8; i++)
				{
					for (int j = 0; j < 8; j++)
					{
						// Pick a square to try. 
						Move *opp_move = new Move(i, j);
						
						// Check validity
						if (temp_board->checkMove(opp_move, otherside))
						{
							int value = temp_board->getValue(opp_move, pside, otherside);
							opp_value.push_back(value);
						}
					}
				}
				
				// If the opponent has no moves after this one, 
				// calculate the value after the player's move.
				if (opp_value.size() == 0)
				{
					int val = board->getValue(new_move, pside, otherside);
					heuristic_map.insert(std::pair<Move*,int>(new_move, val));
				}	
				 
				// Find the minimum value of a move the opponent could
				// make after this move. 
				else {
				    int min = opp_value[0];
				    for (unsigned int i = 1; i < opp_value.size(); ++i)
				    {
						if (opp_value[i] < min)
						{
							min = opp_value[i];
						}
					}
					
					// Add the minimum to the map. 
					heuristic_map.insert(std::pair<Move*,int>(new_move, min));
				}
			}
		}
	}
	
	if(heuristic_map.empty())
	{
		std::cerr << "No moves found" << std::endl;
		board->doMove(NULL, pside);
		return NULL;		
	}
	else
	{
		std::map<Move*, int>::iterator i = heuristic_map.begin();
		
		int max_val = i->second;
		Move* max_move = i->first;
		
		for(; i != heuristic_map.end(); ++i)
		{
			if(i->second > max_val)
			{
				max_val = i->second;
				max_move = i->first;
			}
		}
		
		board->doMove(max_move, pside);
		return max_move;
	}
}
